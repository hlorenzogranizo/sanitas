package com.mycorp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;

import com.mycorp.soporte.BeneficiarioPolizas;
import com.mycorp.soporte.DatosAltaAsegurados;
import com.mycorp.soporte.ExcepcionContratacion;
import com.mycorp.soporte.FrecuenciaEnum;
import com.mycorp.soporte.ProductoPolizas;
import com.mycorp.soporte.RESTResponse;
import com.mycorp.soporte.SimulacionWS;

import es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.InfoContratacion;
import es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Simulacion;
import es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.TarifaBeneficiario;
import es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Tarificacion;
import wscontratacion.contratacion.fuentes.parametros.DatosAlta;
import wscontratacion.contratacion.fuentes.parametros.DatosAsegurado;
import wscontratacion.contratacion.fuentes.parametros.DatosDomicilio;
import wscontratacion.contratacion.fuentes.parametros.DatosPersona;


/**
 * Unit test for simple App.
 */
public class RealizarSimulacionTest  {

    private static final String FECHAALTA = "01/05/2018";
    RealizarSimulacion realizarSimulacion = new RealizarSimulacion();
    DatosAlta datosAltaMock = Mockito.mock(DatosAlta.class);
    DatosDomicilio datosDomicilioMock = Mockito.mock(DatosDomicilio.class);
    InfoContratacion infoContratacionMock = Mockito.mock(InfoContratacion.class);
    Tarificacion tarificacionMock = Mockito.mock(Tarificacion.class);
    SimulacionWS servicioSimulacionMock = Mockito.mock(SimulacionWS.class);
    Simulacion simulacionMock = Mockito.mock(Simulacion.class);
    TarifaBeneficiario ben1 = Mockito.mock(TarifaBeneficiario.class);
    List< ProductoPolizas > lProductosMock = new ArrayList<ProductoPolizas>();
    List< BeneficiarioPolizas > lBeneficiariosMock = new ArrayList<BeneficiarioPolizas>();
    DatosAltaAsegurados datosAltaAseguradosMock = Mockito.mock(DatosAltaAsegurados.class);
    boolean desglosarMock;
    Map< String, Object > hmValores = new HashMap<String, Object>();
    final  DatosAsegurado datosAseguradoMock = Mockito.mock(DatosAsegurado.class);
    final  DatosPersona datosPersonaMock = Mockito.mock(DatosPersona.class);
    es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Tarifas tarMock = Mockito.mock(es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Tarifas.class);

    @Test
    public void testGeneral() throws ExcepcionContratacion, Exception {
        initMockitoDatosAlta();
        initMocitoDatosDomicilio();
        Whitebox.setInternalState(realizarSimulacion, "servicioSimulacion", servicioSimulacionMock);
        RESTResponse< Tarificacion, es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Error > resp = new RESTResponse< Tarificacion, es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Error > ();
        resp.error = new  es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Error();
        TarifaBeneficiario[] tarBen = new TarifaBeneficiario[1] ;        
        resp.out =tarificacionMock;
        Mockito.when(servicioSimulacionMock.simular(Matchers.anyObject())).thenReturn(resp);
        Mockito.when(tarificacionMock.getTarifas()).thenReturn(tarMock);
        Mockito.when(tarificacionMock.getTarifas().getTarifaBeneficiarios()).thenReturn(tarBen);        
      realizarSimulacion.realizarSimulacion(datosAltaMock, lProductosMock, lBeneficiariosMock, false,hmValores);

    }

    @Test
    public void testobtenerInfoContratacion() throws ExcepcionContratacion, Exception {
        initMockitoDatosAlta();
        initMocitoDatosDomicilio();
        InfoContratacion infCOntratacion = realizarSimulacion.obtenerInfoContratacion(datosAltaMock, lBeneficiariosMock, lProductosMock, FrecuenciaEnum.ANUAL, 1);
        Assert.assertEquals(infCOntratacion.getFechaEfecto(), FECHAALTA);
    }

    @Test
    public void testInclusionBeneficiario() throws ExcepcionContratacion, Exception {
        initMockitoDatosAlta();
        initMocitoDatosDomicilio();
        realizarSimulacion.inclusion(datosAltaAseguradosMock, new Integer(2), infoContratacionMock, new Long(100));
        Mockito.verify(infoContratacionMock,Mockito.times(1)).setIdPoliza(100);

    }


    @Test
    public void testInclusionBeneficiarioProducto() throws ExcepcionContratacion, Exception {
        initMockitoDatosAlta();
        initMocitoDatosDomicilio();
        Mockito.when(datosAltaAseguradosMock.getIdEmpresa()).thenReturn(null);
        realizarSimulacion.inclusion(datosAltaAseguradosMock, new Integer(3), infoContratacionMock, new Long(100));
        Mockito.verify(infoContratacionMock,Mockito.times(0)).setIdPoliza(100);

    }

    @Test
    public void testInclusionBeneficiarioPolizasColectivas() throws ExcepcionContratacion, Exception {
        initMockitoDatosAlta();
        initMocitoDatosDomicilio();
        Mockito.when(datosAltaAseguradosMock.getIdDepartamento()).thenReturn(1);
        realizarSimulacion.inclusion(datosAltaAseguradosMock, new Integer(3), infoContratacionMock, new Long(100));
        Mockito.verify(infoContratacionMock,Mockito.times(1)).setIdDepartamento(1);

    }

    @Test
    public void testprocesarTitular() throws ExcepcionContratacion, Exception {
        String fechaNacimiento = "25/11/1982";
        initMockitoDatosAlta();
        es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Beneficiario beneficiario = realizarSimulacion.procesarTitular(datosAltaMock);
        Assert.assertEquals(beneficiario.getFechaNacimiento(), fechaNacimiento);
    }



    private void initMockitoDatosAlta() {
        Mockito.when(datosAltaMock.getGenFrecuenciaPago()).thenReturn(1);
        List<DatosDomicilio> domicilios = new ArrayList<DatosDomicilio>();
        domicilios.add(datosDomicilioMock);
        Mockito.when(datosAltaMock.getDomicilios()).thenReturn(domicilios);
        Mockito.when(datosAltaMock.getTitular()).thenReturn(datosAseguradoMock);
        Mockito.when(datosAltaMock.getTitular().getDatosPersonales()).thenReturn(datosPersonaMock);
        Mockito.when(datosAltaMock.getTitular().getDatosPersonales().getFNacimiento()).thenReturn("25/11/1982");
        Mockito.when(datosAltaMock.getFAlta()).thenReturn(FECHAALTA);
    }

    private void initMocitoDatosDomicilio() {
        Mockito.when(datosDomicilioMock.getCodPostal()).thenReturn(28012);

    }
    
    @Test(expected = ExcepcionContratacion.class)
    public void testGeneralExcepcionContratacion() throws ExcepcionContratacion, Exception {
        initMockitoDatosAlta();
        initMocitoDatosDomicilio();
        Whitebox.setInternalState(realizarSimulacion, "servicioSimulacion", servicioSimulacionMock);
        RESTResponse< Tarificacion, es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Error > resp = new RESTResponse< Tarificacion, es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Error > ();
        resp.error = new  es.sanitas.seg.simulacionpoliza.services.api.simulacion.vo.Error();
        resp.out = Mockito.mock(Tarificacion.class);
        Mockito.when(servicioSimulacionMock.simular(Matchers.anyObject())).thenReturn(resp);
      realizarSimulacion.realizarSimulacion(datosAltaMock, lProductosMock, lBeneficiariosMock, false,hmValores);

    }


}
